import pandas as pd
import snscrape.modules.twitter as sntwitter
import itertools
from IPython.display import display

tweets = sntwitter.TwitterSearchScraper('"christmas"').get_items()
gdanskTweets = sntwitter.TwitterSearchScraper('christmas near:"Gdansk" within 10km').get_items()

df = pd.DataFrame(itertools.islice(tweets, 100))
dfGdansk = pd.DataFrame(itertools.islice(gdanskTweets, 50))
print(df)
print(dfGdansk)




