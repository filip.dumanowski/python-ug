import pandas as pd
from nltk import RegexpTokenizer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import implicits
#import zad1

with open("data.txt", encoding="UTF-8") as file:
    text1 = file.read()
with open("data6part1.txt", encoding="UTF-8") as file:
    text2 = file.read()
with open("data6part2.txt", encoding="UTF-8") as file:
    text3 = file.read()

token = RegexpTokenizer(r'[a-zA-Z0-9]+')
texts = (text1, text2, text3)
stop_words = set(stopwords.words("english"))
stop_words = stop_words.union({'.', ',', '\'', '%', '-', '"', '``', '`', '\'s', '\'\'', '’'})

cv = CountVectorizer(lowercase=True,stop_words='english', ngram_range = (1,1),tokenizer = token.tokenize)

text1 = [text1]
text_counts= cv.fit_transform(text1)
text_array = text_counts.toarray()

print(text_counts)
df = pd.DataFrame(data=text_array,columns = cv.get_feature_names_out())

tfidf_transformer = TfidfTransformer()
tfidf = tfidf_transformer.fit_transform(text_array).toarray()




#for text in texts:
#    zad1.textAnalysis(text[0])
