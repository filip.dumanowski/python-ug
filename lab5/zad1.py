import nltk
from collections import Counter
import matplotlib.pyplot as plt
from nltk import word_tokenize, WordNetLemmatizer
from nltk.corpus import stopwords
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator


nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('omw-1.4')

with open("data.txt", encoding="UTF-8") as file:
    text = file.read()


def textAnalysis(text):
    tokenized_word=word_tokenize(text)
    print(tokenized_word)
    words_count=len(tokenized_word)

    stop_words=set(stopwords.words("english"))
    #print([w for w in tokenized_word if len(w) == 1])
    stop_words = stop_words.union({'.', ',', '\'', '%', '-', '"', '``', '`', '\'s', '\'\'', '’'})

    filtered = []
    for w in tokenized_word:
        if w not in stop_words:
            filtered.append(w)
    print(filtered)

    lemmatized = []
    lem = WordNetLemmatizer()
    for w in filtered:
        lemmatized.append(lem.lemmatize(w))
    counter = Counter(lemmatized)
    print(counter)

    common_words = counter.most_common(10)
    words = [w for w, _ in common_words]
    occurances = [n for _, n in common_words]
    plt.bar(words, occurances)
    plt.show()
    wordcloud = WordCloud(stopwords=stop_words, background_color="white").generate(text)

    # Display the generated image:
    # the matplotlib way:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()


textAnalysis(text)


