import pandas as pd
import matplotlib.pyplot as plt
import math
import random
import statistics

#ZAD1
def prime(number):
    if number > 1:
        for j in range(2, int(number/2)+1):
            if (number % j) == 0:
                return False
            else:
                return True
    else:
        return False

def selectprime(numbers):
    result = []
    for number in numbers:
        if prime(number):
            result.append(number)
    return result

print(prime(5))
print(selectprime([1, 2, 5, 7, 8, 12]))



#ZAD2
x = [3, 8, 9, 10, 12]
y = [8, 8, 7, 5, 6]
dotProduct = 0
xlength = 0
ylength = 0
multiply = []
sum = []
randVector = []
normalizedRandVector = []
standarizedRandVector = []
randVectorSum = 0

for xel, yel in zip(x, y):
	multiply.append(xel * yel)

for xel, yel in zip(x, y):
	sum.append(xel + yel)

for xel, yel in zip(x, y):
    dotProduct += (xel * yel)

for cord in x:
    xlength += pow(cord, 2)
xlength = math.sqrt(xlength)

for cord in y:
    ylength += pow(cord, 2)
ylength = math.sqrt(ylength)

for i in range(1, 50):
    randVector.append(random.randint(1, 100))

randVectorMean = statistics.mean(randVector)
for i in randVector:
    randVectorSum += i
randVectorMin = min(randVector)
randVectorMax = max(randVector)
randVectorStDev = statistics.stdev(randVector)

for cord in randVector:
    normalizedRandVector.append((cord - randVectorMin)/(randVectorMax - randVectorMin))

for cord in randVector:
    standarizedRandVector.append((cord - randVectorMean)/randVectorStDev)

standarizedRandVectorMean = statistics.mean(standarizedRandVector)
standarizedRandVectorStDev = statistics.stdev(standarizedRandVector)

print("multiply: ", multiply)
print("sum: ", sum)
print("dotproduct: ", dotProduct)
print("xlength: ", xlength)
print("ylength: ", ylength)
print("randvector: ", randVector)
print("mean: ", randVectorMean)
print("sum: : ", randVectorSum)
print("min: ", randVectorMin)
print("max: ", randVectorMax)
print("stdev: ", randVectorStDev)
print("stdrandvector: ", standarizedRandVector)
print("normrandvector: ", normalizedRandVector)
print("mean: ", standarizedRandVectorMean)
print("stdev: ", standarizedRandVectorStDev)

#ZAD3
df = pd.read_csv('miasta.csv')
df.loc[10, :] = [2010, 460, 555, 405]
print(df)
x = df['Rok']

plt.plot(x, df['Gdansk'], 'r-o')
plt.plot(x, df['Poznan'], 'g-o')
plt.plot(x, df['Szczecin'], 'b-o')
plt.title('Ludnosc')
plt.xlabel('Lata')
plt.ylabel('Ludnosc [w tys.]')
plt.show()
