import pyswarms as ps
from matplotlib import pyplot as plt
from pyswarms.utils.functions import single_obj as fx
from pyswarms.utils.plotters import plot_cost_history
from pyswarms.backend.topology import Star, Pyramid

#ZAD1
# Set-up hyperparameters
options = {'c1': 0.5, 'c2': 0.3, 'w':0.9}

# Call instance of GlobalBestPSO
optimizer = ps.single.GlobalBestPSO(n_particles=10, dimensions=2,
                                    options=options)

# Perform optimization
stats = optimizer.optimize(fx.sphere, iters=100)
plot_cost_history(optimizer.cost_history)
plt.plot(optimizer.cost_history)

my_topology = Star()
optimizer = ps.single.GeneralOptimizerPSO(n_particles=10, dimensions=2,
                                    options=options, topology=my_topology)
stats = optimizer.optimize(fx.sphere, iters=100)
plt.plot(optimizer.cost_history)

my_topology = Pyramid(static=False)
optimizer = ps.single.GeneralOptimizerPSO(n_particles=10, dimensions=2,
                                    options=options, topology=my_topology)
stats = optimizer.optimize(fx.sphere, iters=100)
plt.plot(optimizer.cost_history)
plt.show()

#ZAD2



