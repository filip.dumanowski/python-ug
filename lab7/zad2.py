import pandas as pd
from sklearn.datasets import load_iris

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split

iris = load_iris()

datasets = train_test_split(iris.data, iris.target,
                            test_size=0.3)

train_data, test_data, train_labels, test_labels = datasets

clf = MLPClassifier(solver='lbfgs',
                    alpha=1e-5,
                    hidden_layer_sizes=(2,),
                    random_state=1)

clf.fit(train_data, train_labels)
print(clf.score(test_data, test_labels))

clf = MLPClassifier(solver='lbfgs',
                    alpha=1e-5,
                    hidden_layer_sizes=(3,),
                    random_state=1)

clf.fit(train_data, train_labels)
print(clf.score(test_data, test_labels))