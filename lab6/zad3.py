import pandas as pd
from sklearn import tree
from sklearn.metrics import confusion_matrix

from sklearn.model_selection import train_test_split
df = pd.read_csv("iris.csv")
(train_set, test_set) = train_test_split(df.values, train_size=0.7, random_state=13)
clf = tree.DecisionTreeClassifier()
clf = clf.fit(train_set[1:,:3], train_set[1:,4])
tree.plot_tree(clf)

good_predictions = 0
len = test_set.shape[0]
results = clf.predict(test_set[:, :3])
for i in range(len):
    if results[i] == test_set[i, 4]:
        good_predictions = good_predictions + 1

confusionmatrix = confusion_matrix(test_set[:,4], results)

print(good_predictions / len * 100, "%")
print(confusionmatrix)

#EXACTLY THE SAME :)
