import pandas as pd
from sklearn.datasets import load_iris
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB

df = pd.read_csv("iris.csv")
(train_set, test_set) = train_test_split(df.values, train_size=0.7, random_state=13)


gnb = GaussianNB()
results = gnb.fit(train_set[:,:3], train_set[:,4]).predict(test_set[:,:3])

len = test_set.shape[0]
good_predictions = 0
for i in range(len):
    if results[i] == test_set[i, 4]:
        good_predictions = good_predictions + 1

confusionmatrix = confusion_matrix(test_set[:,4], results)

print(good_predictions / len * 100, "%")
print(confusionmatrix)