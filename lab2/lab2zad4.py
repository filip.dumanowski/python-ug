import pygad
import math
import time

labirynth = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
             [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1],
             [1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
             [1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1],
             [1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
             [1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1],
             [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1],
             [1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1],
             [1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1],
             [1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1],
             [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]

# definiujemy parametry chromosomu
# geny to liczby: 0 lub 1
gene_space = [1, 2, 3, 4]

# definiujemy funkcję fitness
def fitness_func(solution, solution_idx):
    #Starting position
    x = 1
    y = 1
    xTemp = 1
    yTemp = 1

    for i in solution:
        if i == 1:
            yTemp = y - 1
        elif i == 2:
            xTemp = x + 1
        elif i == 3:
            yTemp = y + 1
        elif i == 4:
            xTemp = x - 1

        #If choosen position is correct (empty)
        if labirynth[yTemp][xTemp] == 0:
            x = xTemp
            y = yTemp

        xTemp = x
        yTemp = y
        
    fitness = math.sqrt(pow((10 - y), 2) + pow((10 - x), 2))
    return -fitness


fitness_function = fitness_func

# ile chromsomów w populacji
# ile genow ma chromosom
sol_per_pop = 30
num_genes = 30

# ile wylaniamy rodzicow do "rozmanazania" (okolo 50% populacji)
# ile pokolen
# ilu rodzicow zachowac (kilka procent)
num_parents_mating = 20
num_generations = 300
keep_parents = 2

# jaki typ selekcji rodzicow?
# sss = steady, rws=roulette, rank = rankingowa, tournament = turniejowa
parent_selection_type = "sss"

# w il =u punktach robic krzyzowanie?
crossover_type = "single_point"

# mutacja ma dzialac na ilu procent genow?
# trzeba pamietac ile genow ma chromosom
mutation_type = "random"
mutation_percent_genes = 12

# inicjacja algorytmu z powyzszymi parametrami wpisanymi w atrybuty
ga_instance = pygad.GA(gene_space=gene_space,
                       num_generations=num_generations,
                       num_parents_mating=num_parents_mating,
                       fitness_func=fitness_function,
                       sol_per_pop=sol_per_pop,
                       num_genes=num_genes,
                       parent_selection_type=parent_selection_type,
                       keep_parents=keep_parents,
                       crossover_type=crossover_type,
                       mutation_type=mutation_type,
                       mutation_percent_genes=mutation_percent_genes,
                       stop_criteria=["reach_0"])

# uruchomienie algorytmu
ga_instance.run()

# podsumowanie: najlepsze znalezione rozwiazanie (chromosom+ocena)
solution, solution_fitness, solution_idx = ga_instance.best_solution()
print("Parameters of the best solution : {solution}".format(solution=solution))
print("Fitness value of the best solution = {solution_fitness}".format(solution_fitness=solution_fitness))
print("Number of generations passed is {generations_completed}".format(
    generations_completed=ga_instance.generations_completed))

# wyswietlenie wykresu: jak zmieniala sie ocena na przestrzeni pokolen
ga_instance.plot_fitness()

timeSum = 0
for i in range(1,10):
    start = time.time()
    ga_instance.run()
    stop = time.time()
    timeSum += stop - start
print(timeSum/10)