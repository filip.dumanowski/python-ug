import matplotlib.pyplot as plt
import simpful
from simpful import FuzzySystem, TrapezoidFuzzySet, LinguisticVariable, TriangleFuzzySet

FS = FuzzySystem()

O1 = TrapezoidFuzzySet(0,0,20,45,   term="m")
O2 = TrapezoidFuzzySet(20,45,105,130,  term="s")
O3 = TrapezoidFuzzySet(105,130,140,140, term="d")
FS.add_linguistic_variable("odleglosc", LinguisticVariable([O1, O2, O3], universe_of_discourse=[0,140]))

O1 = TrapezoidFuzzySet(0, 0, 15, 65,   term="m")
O2 = TriangleFuzzySet(15, 65, 115,  term="s")
O3 = TrapezoidFuzzySet(65, 115, 120, 120, term="d")
FS.add_linguistic_variable("predkosc", LinguisticVariable([O1, O2, O3], universe_of_discourse=[0,120]))

O1 = TrapezoidFuzzySet(-1,-1,-0.4, 0, term="DM") #
O2 = TriangleFuzzySet(-0.4,0,0.1,  term="MM")
O3 = TriangleFuzzySet(-0.1,0,0.4, term="MP")
O4 = TrapezoidFuzzySet(0, 0.4,1, 1,term="DP")
FS.add_linguistic_variable("przyspieszenie", LinguisticVariable([O1, O2, O3, O4], universe_of_discourse=[-1,1]))

FS.add_rules([
	"IF (odleglosc IS m) AND (predkosc IS m) THEN (przyspieszenie IS DM)",
	"IF (odleglosc IS s) AND (predkosc IS m) THEN (przyspieszenie IS MM)",
	"IF (odleglosc IS d) AND (predkosc IS m) THEN (przyspieszenie IS MM)",
	"IF (odleglosc IS m) AND (predkosc IS s) THEN (przyspieszenie IS DP)",
	"IF (odleglosc IS s) AND (predkosc IS s) THEN (przyspieszenie IS MM)",
	"IF (odleglosc IS d) AND (predkosc IS s) THEN (przyspieszenie IS MM)",
	"IF (odleglosc IS m) AND (predkosc IS d) THEN (przyspieszenie IS MP)",
	"IF (odleglosc IS s) AND (predkosc IS d) THEN (przyspieszenie IS DM)",
	"IF (odleglosc IS d) AND (predkosc IS d) THEN (przyspieszenie IS MM)",
	])
FS.plot_variable("odleglosc")
FS.plot_variable("predkosc")
FS.plot_variable("przyspieszenie")

FS.set_variable("odleglosc", 30)
FS.set_variable("predkosc", 50)
przyspieszenie = FS.inference()
print(przyspieszenie)

FS.set_variable("odleglosc", 100)
FS.set_variable("predkosc", 50)
przyspieszenie = FS.inference()
print(przyspieszenie)

FS.set_variable("odleglosc", 30)
FS.set_variable("predkosc", 65)
przyspieszenie = FS.inference()
print(przyspieszenie)

